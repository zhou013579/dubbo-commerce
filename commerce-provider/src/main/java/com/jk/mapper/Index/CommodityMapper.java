package com.jk.mapper.Index;

import com.jk.model.commodity.Commodity;

import java.util.List;

public interface CommodityMapper {

    List<Commodity> queryCommodity(Commodity commodity);
}
