package com.jk.mapper.zy;

import com.jk.model.zy.Vehicle;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CheckoutMapper {
    List<Vehicle> selectvehicle();

    void delve(@Param("id") Integer id);

    void addcheck(@Param("count")Integer count, @Param("pricesums")Integer pricesums,@Param("id")Integer id);
}
