package com.jk.app;

import com.alibaba.dubbo.spring.boot.annotation.EnableDubboConfiguration;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@EnableCaching
@SpringBootApplication
@EnableDubboConfiguration
@ComponentScan("com.jk.*")
@MapperScan(basePackages = "com.jk.mapper.*")
public class CommerceProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(CommerceProviderApplication.class, args);
    }

}

