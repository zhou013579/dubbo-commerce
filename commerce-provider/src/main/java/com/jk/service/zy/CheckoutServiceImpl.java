package com.jk.service.zy;

import com.alibaba.dubbo.config.annotation.Service;
import com.jk.mapper.zy.CheckoutMapper;
import com.jk.model.zy.Vehicle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Service(interfaceClass = CheckoutService.class )
@Component
public class CheckoutServiceImpl implements CheckoutService{

    @Autowired
    private CheckoutMapper checkoutMapper;

    @Override
    public List<Vehicle> selectvehicle() {
        return checkoutMapper.selectvehicle();
    }

    @Override
    public void delve(Integer id) {
        checkoutMapper.delve(id);
    }

    @Override
    public void addcheck(Integer count, Integer pricesums,Integer id) {
        checkoutMapper.addcheck(count,pricesums,id);
    }
}
