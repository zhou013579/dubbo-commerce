package com.jk.service.Index;

import com.alibaba.dubbo.config.annotation.Service;
import com.jk.mapper.Index.CommodityMapper;
import com.jk.model.commodity.Commodity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Service(interfaceClass = CommodityService.class )
@Component
public class CommodityServiceImpl implements CommodityService {
  @Autowired
  private CommodityMapper commodityMapper;
    @Override
    public List<Commodity> queryCommodity(Commodity commodity) {
        List<Commodity> list = commodityMapper.queryCommodity(commodity);
        return list;
    }
}
