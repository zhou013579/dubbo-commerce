package com.jk.controller.page;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("page")
public class PageController {


    @RequestMapping("toIndex")
    public String toindex() {
        return "index";
    }

    @RequestMapping("toAccount")
    public String toAccount() {
        return "account";
    }

    @RequestMapping("toCheckout")
    public String toCheckout() {
        return "checkout";
    }

    @RequestMapping("toContact")
    public String toContact() {
        return "contact";
    }

    @RequestMapping("toLogin")
    public String toLogin() {
        return "login";
    }

    @RequestMapping("toProduct")
    public String toProduct() {
        return "product";
    }

    @RequestMapping("toSingle")
    public String toSingle() {
        return "single";
    }

    @RequestMapping("toTypo")
    public String toTypo() {
        return "typo";
    }

}
