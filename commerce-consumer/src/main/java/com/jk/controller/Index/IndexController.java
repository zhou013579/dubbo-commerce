package com.jk.controller.Index;


import com.alibaba.dubbo.config.annotation.Reference;
import com.jk.model.commodity.Commodity;
import com.jk.service.Index.CommodityService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("index")
public class IndexController {

    @Reference
    private CommodityService commodityService;

    //查询商品表
    @RequestMapping("queryCommodity")
    @ResponseBody
    public List<Commodity> queryCommodity(Commodity commodity){
       List<Commodity> list = commodityService.queryCommodity(commodity);
        return list;
    }
}