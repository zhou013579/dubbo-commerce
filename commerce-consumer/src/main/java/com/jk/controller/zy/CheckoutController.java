package com.jk.controller.zy;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jk.model.zy.Vehicle;
import com.jk.service.zy.CheckoutService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("check")
public class CheckoutController {

    @Reference
    private CheckoutService checkoutService;

    @RequestMapping("selectvehicle")
    @ResponseBody
    public List<Vehicle> selectvehicle(){
        List<Vehicle> list=checkoutService.selectvehicle();
        return list;
    }


    @RequestMapping("delve")
    @ResponseBody
    public void delve(Integer id) {
        checkoutService.delve(id);
    }


    @RequestMapping("addcheck")
    public String addcheck(Integer count,Integer pricesums,Integer id){
        checkoutService.addcheck(count,pricesums,id);
        return "index";
    }
}
