package com.jk.app;

import com.alibaba.dubbo.spring.boot.annotation.EnableDubboConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@EnableDubboConfiguration
@ComponentScan("com.jk.*")
@SpringBootApplication
public class CommerceConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(CommerceConsumerApplication.class, args);
    }

}

