package com.jk.model.zy;

import lombok.Data;

import java.io.Serializable;

@Data
public class Vehicle implements Serializable {

    private Integer veid;

    private Integer userid;

    private Integer commodityid;

    private String  commodityname;

    private Integer specid;

    private String picture;

    private Integer repertorycont;

    private Integer price;

    private Integer pricesum;

    private String comtype;

    private Integer carriage;

    private String arrivaldate;



}
