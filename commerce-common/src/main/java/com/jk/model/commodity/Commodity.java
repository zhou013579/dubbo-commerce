package com.jk.model.commodity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Commodity implements Serializable {

    //    private Integer commodityId;//商品主键
//    private String commodityNo;//商品编号
//    private String commodityName;//商品名称
//    private Integer commodityBrandId;//品牌名称
//    private Integer commodityClassId;//商品分类id
//    private Integer commodityUnitId;//单位id
//    private Integer commodityColorId;//颜色id
//    private Integer commoditySpecificationId;//规格id
//    private String commodityCourse;//课程
//    private Double commodityDefaultPrice;//默认价格
//    private String commodityState;//状态
//    private String commodityCover;//封面
//    private String commodityImgtext;//图文
//    private String commodityVideo;//视频




    private Integer commodityId;

    private String commodityNo;

    private String commodityName;

    private Integer commodityBrandId;
    private String commodityBrandName;

    private Integer commodityClassId;
    private String commodityClassName;

    private Integer commodityUnitId;
    private String commodityUnitName;

    private Integer commodityColorId;
    private String commodityColorName;

    private Integer commoditySpecificationId;
    private String commoditySpecificationName;

    private String commodityCourse;

    private Double commodityDefaultPrice;

    private String commodityState;

    private String commodityCover;

    private String commodityImgtext;

    private String commodityVideo;

    private String commodityPrice;

    private String commodityOldPrice;

    private String commodityDiscount;
}
